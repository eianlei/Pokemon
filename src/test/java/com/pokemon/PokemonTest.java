package com.pokemon;

import static org.junit.Assert.*;

import java.util.Comparator;

import org.junit.Before;
import org.junit.Test;

public class PokemonTest {
	private Pokemon pokemon;
	
	@Before
	public void initPokemon() {
		pokemon = new Pokemon(54, "psyduck", 8, 196);
	}
	
	@Test
	public void testPokemonGetters() {
		assertEquals(54, pokemon.getId());
		assertEquals("Psyduck", pokemon.getName());
		assertEquals(8, pokemon.getHeight());
		assertEquals(196, pokemon.getWeight());
	}
	
	@Test
	public void testPokemonSetId() {
		pokemon.setId(101);
		assertEquals(101, pokemon.getId());
	}
	
	@Test
	public void testPokemonSetName() {
		pokemon.setName("pikachu");
		assertEquals("Pikachu", pokemon.getName());
	}
	
	@Test
	public void testPokemonSetHeight() {
		pokemon.setHeight(101);
		assertEquals(101, pokemon.getHeight());
	}
	
	@Test
	public void testPokemonSetWeight() {
		pokemon.setWeight(101);
		assertEquals(101, pokemon.getWeight());
	}
	
	@Test
	public void testToString() {
		String result = "054 Psyduck       height:   8    weight:  196";
		assertEquals(result, pokemon.toString());
	}
	
	@Test
	public void testComparator() {
		Comparator<Pokemon> comparator = Pokemon.HeightComparator;
		Pokemon pokemon2 = new Pokemon (54, "tallPsyduck1", 18, 196);
		Pokemon pokemon3 = new Pokemon (54, "tallPsyduck2", 18, 196);
		
		assertTrue(comparator.compare(pokemon, pokemon2) > 0);
		assertTrue(comparator.compare(pokemon2, pokemon) < 0);
		assertTrue(comparator.compare(pokemon2, pokemon3) == 0);
	}
}
