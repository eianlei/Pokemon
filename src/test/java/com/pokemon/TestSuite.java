package com.pokemon;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	PokemonTest.class,
	PokemonListTest.class,
	PokemonServiceTest.class
})

public class TestSuite {

}
