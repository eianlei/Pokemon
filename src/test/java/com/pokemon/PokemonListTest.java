package com.pokemon;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PokemonListTest {
	private PokemonList pokemonList;
	private Pokemon pokemon1, pokemon2, pokemon3;
	
	@Before
	public void initPokemonList() {
		pokemon1 = new Pokemon(54, "psyduck", 8, 196);
		pokemon2 = new Pokemon(4, "charmander", 6, 9);
		pokemon3 = new Pokemon(68, "machamp", 16, 130);
		
		pokemonList = new PokemonList();
		pokemonList.add(pokemon1);
		pokemonList.add(pokemon2);
		pokemonList.add(pokemon3);
	}
	
	@Test
	public void testSortByHeight() {
		pokemonList.sortByHeight();
		assertEquals(pokemon3, pokemonList.get(0));
		assertEquals(pokemon1, pokemonList.get(1));
		assertEquals(pokemon2, pokemonList.get(2));
	}
	
	@Test
	public void testToString() {
		String expected = pokemon1.toString() + "\n" + pokemon2.toString() + "\n" +
				pokemon3.toString() + "\n";
		assertEquals(expected, pokemonList.toString());
	}

}
