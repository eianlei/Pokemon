package com.pokemon;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@RunWith(MockitoJUnitRunner.class)
public class PokemonServiceTest {
	@Mock
	private RestTemplate restTemplate;
	@InjectMocks
	private PokemonService service = new PokemonService();
	
	private void mockRestTemplate(int id, ResponseEntity<Pokemon> response) {
		Mockito.when(restTemplate.exchange(Mockito.eq("http://pokeapi.co/api/v2/pokemon/{id}"), 
				Mockito.eq(HttpMethod.GET), Mockito.any(), Mockito.eq(Pokemon.class), Mockito.eq(id)))
		.thenReturn(response);
	}
	
	@Test
	public void testGetPokemon() {
		int id = 54;
		Pokemon pokemon = new Pokemon(id, "psyduck", 8, 196);
		ResponseEntity<Pokemon> response = new ResponseEntity<Pokemon>(pokemon, HttpStatus.OK);
		
		this.mockRestTemplate(id, response);
		
		Pokemon receivedPokemon = service.getPokemon(id);
		
		assertEquals(pokemon, receivedPokemon);
	}
	
	@Test
	public void testGetPokemons() {
		int id1 = 50;
		int id2 = 51;
		Pokemon pokemon1 = new Pokemon(id1, "psyduck1", 8, 196);
		Pokemon pokemon2 = new Pokemon(id2, "psyduck2", 8, 196);
		ResponseEntity<Pokemon> response1 = new ResponseEntity<Pokemon>(pokemon1, HttpStatus.OK);
		ResponseEntity<Pokemon> response2 = new ResponseEntity<Pokemon>(pokemon2, HttpStatus.OK);
		
		this.mockRestTemplate(id1, response1);
		this.mockRestTemplate(id2, response2);
		
		PokemonList receivedPokemons = service.getPokemons(50, 51);
		
		assertEquals(pokemon1, receivedPokemons.get(0));
		assertEquals(pokemon2, receivedPokemons.get(1));
		assertEquals(2, receivedPokemons.size());
	}

}
