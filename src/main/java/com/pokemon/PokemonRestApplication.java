package com.pokemon;

import java.util.Scanner;

public class PokemonRestApplication {

	public static void main(String[] args) {
		PokemonService service = new PokemonService();
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("ID of the first Pokemon: ");
		int idMin = Integer.parseInt(scanner.nextLine().trim());
		System.out.print("ID of the last Pokemon: ");
		int idMax = Integer.parseInt(scanner.nextLine().trim());
		scanner.close();
		
		PokemonList pokemonList = service.getPokemons(idMin, idMax);
		
		if (pokemonList.isEmpty()) {
			System.out.println("No Pokemons found");
			return;
		}
		
		System.out.println("");
		System.out.println(pokemonList);
		
		pokemonList.sortByHeight();
		System.out.println("Tallest Pokemons:");
		for (int i = 0; i < 10 && i < pokemonList.size(); i++) {
			System.out.println((i + 1) + ". " + pokemonList.get(i).getName());
		}
		
	}
}
