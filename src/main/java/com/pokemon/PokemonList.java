package com.pokemon;

import java.util.ArrayList;
import java.util.Collections;

public class PokemonList extends ArrayList<Pokemon> {
	
	public void sortByHeight() {
		Collections.sort(this, Pokemon.HeightComparator);
	}
	
	public String toString() {
		String result = "";
		for (Pokemon pokemon : this) {
			result += pokemon.toString() + "\n";
		}
		
		return result;
	}
}
