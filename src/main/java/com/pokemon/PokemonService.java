package com.pokemon;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class PokemonService {
	private RestTemplate restTemplate;
	private HttpEntity<?> request;
	
	public PokemonService() {
		this.restTemplate = new RestTemplate();
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("User-Agent", "");
		this.request = new HttpEntity<>(headers);
	}
	
	public Pokemon getPokemon(int id) {
		try {
			ResponseEntity<Pokemon> response = this.restTemplate.exchange("http://pokeapi.co/api/v2/pokemon/{id}", HttpMethod.GET,
				this.request, Pokemon.class, id);
			return response.getBody();
		} catch (HttpStatusCodeException exception) {
			System.err.println("Getting a Pokemon failed with status code " + exception.getStatusCode().value());
			System.exit(1);
		} catch (RestClientException exception) {
			System.err.println(exception);
			System.exit(1);
		}
		return null;
	}
	
	public PokemonList getPokemons(int idMin, int idMax) {
		PokemonList pokemonList = new PokemonList();
		for (int i = idMin; i <= idMax; i++) {
			Pokemon pokemon = getPokemon(i);
			pokemonList.add(pokemon);
		}
		
		return pokemonList;
	}
}
