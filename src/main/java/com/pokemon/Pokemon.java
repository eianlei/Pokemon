package com.pokemon;

import java.util.Comparator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Pokemon {
	private int id;
	private String name;
	private int height;
	private int weight;

	public static Comparator<Pokemon> HeightComparator = new Comparator<Pokemon>() {
		public int compare(Pokemon p1, Pokemon p2) {
			return p2.getHeight() - p1.getHeight();
		}
	};

	public Pokemon() {
	}
	
	public Pokemon(int id, String name, int height, int weight) {
		this.id = id;
		this.height = height;
		this.weight = weight;
		this.setName(name);
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		String formattedName = name.substring(0, 1).toUpperCase() + name.substring(1);
		this.name = formattedName;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getHeight() {
		return height;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getWeight() {
		return weight;
	}

	@Override
	public String toString() {
		return String.format("%03d", this.id) + " " + String.format("%-13s", this.name) + " height:" + 
				String.format("%4d", this.height) + "    weight:" + String.format("%5d", this.weight);
	}

}
